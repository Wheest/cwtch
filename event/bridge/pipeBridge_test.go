package bridge

import (
	"cwtch.im/cwtch/event"
	"testing"
)

var (
	clientPipe  = "./client"
	servicePipe = "./service"
)

func clientHelper(t *testing.T, in, out string, messageOrig *event.IPCMessage, done chan bool) {
	client := NewPipeBridgeClient(in, out)

	messageAfter, ok := client.Read()
	if !ok {
		t.Errorf("Reading from client IPCBridge failed")
		done <- true
		return
	}

	if messageOrig.Dest != messageAfter.Dest {
		t.Errorf("Dest's value differs expected: %v actaul: %v", messageOrig.Dest, messageAfter.Dest)
	}

	if messageOrig.Message.EventType != messageAfter.Message.EventType {
		t.Errorf("EventTypes's value differs expected: %v actaul: %v", messageOrig.Message.EventType, messageAfter.Message.EventType)
	}

	if messageOrig.Message.Data[event.Identity] != messageAfter.Message.Data[event.Identity] {
		t.Errorf("Data[Identity]'s value differs expected: %v actaul: %v", messageOrig.Message.Data[event.Identity], messageAfter.Message.Data[event.Identity])
	}

	done <- true
}

func serviceHelper(t *testing.T, in, out string, messageOrig *event.IPCMessage, done chan bool) {
	service := NewPipeBridgeService(in, out)

	service.Write(messageOrig)

	done <- true
}

func TestPipeBridge(t *testing.T) {

	messageOrig := &event.IPCMessage{Dest: "ABC", Message: event.NewEventList(event.NewPeer, event.Identity, "It is I")}
	serviceDone := make(chan bool)
	clientDone := make(chan bool)

	go clientHelper(t, clientPipe, servicePipe, messageOrig, clientDone)
	go serviceHelper(t, servicePipe, clientPipe, messageOrig, serviceDone)

	<-serviceDone
	<-clientDone
}
