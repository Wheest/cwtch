// +build windows

package bridge

import (
	"cwtch.im/cwtch/event"
	"log"
)

func NewPipeBridgeClient(inFilename, outFilename string) event.IPCBridge {
	log.Fatal("Not supported on windows")
	return nil
}

// NewPipeBridgeService returns a pipe backed IPCBridge for a service
func NewPipeBridgeService(inFilename, outFilename string) event.IPCBridge {
	log.Fatal("Not supported on windows")
	return nil
}
