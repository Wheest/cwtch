package event

// Type captures the definition of many common Cwtch application events
type Type string

// Defining Common Event Types
const (
	StatusRequest        = Type("StatusRequest")
	ProtocolEngineStatus = Type("ProtocolEngineStatus")

	// Attempt to outbound peer with a given remote peer
	// attributes:
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"
	PeerRequest = Type("PeerRequest")

	// RetryPeerRequest
	// Identical to PeerRequest, but allows Engine to make decisions regarding blocked peers
	// attributes:
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"
	RetryPeerRequest = Type("RetryPeerRequest")

	// Blocking Events both Block and Unblock have the same structure
	// attributes:
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"
	BlockPeer   = Type("BlockPeer")
	UnblockPeer = Type("UnblockPeer")

	// Turn on/off blocking of unknown peers (if peers aren't in the contact list then they will be autoblocked
	BlockUnknownPeers = Type("BlockUnknownPeers")
	AllowUnknownPeers = Type("AllowUnknownPeers")

	JoinServer = Type("JoinServer")

	ProtocolEngineStartListen = Type("ProtocolEngineStartListen")
	ProtocolEngineStopped     = Type("ProtocolEngineStopped")

	InvitePeerToGroup = Type("InvitePeerToGroup")

	// a group invite has been received from a remote peer
	// attributes:
	// TimestampReceived [eg time.Now().Format(time.RFC3339Nano)]
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"]
	// GroupInvite: [eg "torv3....."]
	NewGroupInvite = Type("NewGroupInvite")

	// GroupID
	AcceptGroupInvite = Type("AcceptGroupInvite")

	SendMessageToGroup = Type("SendMessagetoGroup")

	//Ciphertext, Signature:
	EncryptedGroupMessage = Type("EncryptedGroupMessage")
	//TimestampReceived, TimestampSent, Data(Message), GroupID, Signature, PreviousSignature, RemotePeer
	NewMessageFromGroup = Type("NewMessageFromGroup")

	// an error was encountered trying to send a particular Message to a group
	// attributes:
	// GroupServer: The server the Message was sent to
	// Signature: The signature of the Message that failed to send
	// Error: string describing the error
	SendMessageToGroupError = Type("SendMessageToGroupError")

	SendMessageToPeer  = Type("SendMessageToPeer")
	NewMessageFromPeer = Type("NewMessageFromPeer")

	// Peer acknowledges a previously sent message
	// attributes
	// EventID: The original event id that the peer is responding too.
	// RemotePeer: The peer associated with the acknowledgement
	PeerAcknowledgement = Type("PeerAcknowledgement")

	// attributes:
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"]
	// Error: string describing the error
	SendMessageToPeerError = Type("SendMessageToPeerError")

	// REQUESTS TO STORAGE ENGINE

	// a peer contact has been added
	// attributes:
	// RemotePeer [eg ""]
	PeerCreated = Type("PeerCreated")

	// a group has been successfully added or newly created
	// attributes:
	// Data [serialized *model.Group]
	GroupCreated = Type("GroupCreated")

	// RemotePeer
	DeleteContact = Type("DeleteContact")

	// GroupID
	DeleteGroup = Type("DeleteGroup")

	// change the .Name attribute of a profile (careful - this is not a custom attribute. it is used in the underlying protocol during handshakes!)
	// attributes:
	// ProfileName [eg "erinn"]
	SetProfileName = Type("SetProfileName")

	// request to store a profile-wide attribute (good for e.g. per-profile settings like theme prefs)
	// attributes:
	// Key [eg "fontcolor"]
	// Data [eg "red"]
	SetAttribute = Type("SetAttribute")

	// request to store a per-contact attribute (e.g. display names for a peer)
	// attributes:
	// RemotePeer [eg ""]
	// Key [eg "nick"]
	// Data [eg "erinn"]
	SetPeerAttribute = Type("SetPeerAttribute")

	// request to store a per-cwtch-group attribute (e.g. display name for a group)
	// attributes:
	// GroupID [eg ""]
	// Key [eg "nick"]
	// Data [eg "open privacy board"]
	SetGroupAttribute = Type("SetGroupAttribute")

	// RemotePeer
	// ConnectionState
	PeerStateChange = Type("PeerStateChange")

	// GroupServer
	// ConnectionState
	ServerStateChange = Type("ServerStateChange")

	/***** Application client / service messages *****/

	// ProfileName, Password
	CreatePeer = Type("CreatePeer")

	// service -> client: Identity(localId), Password, [Status(new/default=blank || from reload='running')]
	// app -> Identity(onion)
	NewPeer = Type("NewPeer")

	// Identity(onion), Data(pluginID)
	AddPeerPlugin = Type("AddPeerPlugin")

	// Password
	LoadProfiles = Type("LoadProfiles")

	// Client has reloaded, triggers NewPeer s then ReloadDone
	ReloadClient = Type("ReloadClient")

	ReloadDone = Type("ReloadDone")

	// Identity - Ask service to resend all connection states
	ReloadPeer = Type("ReloadPeer")

	// Identity(onion)
	ShutdownPeer = Type("ShutdownPeer")

	Shutdown = Type("Shutdown")

	// Error(err)
	PeerError = Type("PeerError")

	// Error(err)
	AppError = Type("AppError")

	// Progress, Status
	ACNStatus = Type("ACNStatus")
)

// Field defines common event attributes
type Field string

// Defining Common Field Types
const (
	RemotePeer        = Field("RemotePeer")
	Ciphertext        = Field("Ciphertext")
	Signature         = Field("Signature")
	PreviousSignature = Field("PreviousSignature")
	TimestampSent     = Field("TimestampSent")
	TimestampReceived = Field("TimestampReceived")

	Identity = Field("Identity")

	GroupID     = Field("GroupID")
	GroupServer = Field("GroupServer")
	GroupInvite = Field("GroupInvite")

	ProfileName = Field("ProfileName")
	Password    = Field("Password")

	ConnectionState = Field("ConnectionState")

	Key  = Field("Key")
	Data = Field("Data")

	Error = Field("Error")

	Progreess    = Field("Progress")
	Status       = Field("Status")
	EventID      = Field("EventID")
	EventContext = Field("EventContext")
)

// Defining Common errors
const (
	AppErrLoaded0 = "Loaded 0 profiles"
)

// Defining Protocol Contexts
const (
	ContextAck    = "im.cwtch.acknowledgement"
	ContextInvite = "im.cwtch.invite"
	ContextRaw    = "im.cwtch.raw"
)
