module cwtch.im/cwtch

require (
	cwtch.im/tapir v0.1.10
	git.openprivacy.ca/openprivacy/libricochet-go v1.0.6
	github.com/c-bata/go-prompt v0.2.3
	github.com/golang/protobuf v1.3.2
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mattn/go-tty v0.0.0-20190424173100-523744f04859 // indirect
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/struCoder/pidusage v0.1.2
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
)
