package model

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"testing"
	"time"
)

func TestGroup(t *testing.T) {
	g, _ := NewGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	dgm := &protocol.DecryptedGroupMessage{
		Onion:              proto.String("onion"),
		Text:               proto.String("Hello World!"),
		Timestamp:          proto.Int32(int32(time.Now().Unix())),
		SignedGroupId:      []byte{},
		PreviousMessageSig: []byte{},
		Padding:            []byte{},
	}
	encMessage, _ := g.EncryptMessage(dgm)
	ok, message := g.DecryptMessage(encMessage)
	if !ok || message.GetText() != "Hello World!" {
		t.Errorf("group encryption was invalid, or returned wrong message decrypted:%v message:%v", ok, message)
		return
	}
	g.SetAttribute("test", "test_value")
	value, exists := g.GetAttribute("test")
	if !exists || value != "test_value" {
		t.Errorf("Custom Attribute Should have been set, instead %v %v", exists, value)
	}
	t.Logf("Got message %v", message)
}

func TestGroupErr(t *testing.T) {
	_, err := NewGroup("not a real group name")
	if err == nil {
		t.Errorf("Group Setup Should Have Failed")
	}
}
