package plugins

import (
	"cwtch.im/cwtch/event"
)

// PluginID is used as an ID for signaling plugin activities
type PluginID int

// These are the plugin IDs for the supplied plugins
const (
	CONTACTRETRY PluginID = iota
)

// Plugin is the interface for a plugin
type Plugin interface {
	Start()
	Shutdown()
}

// Get is a plugin factory for the requested plugin
func Get(id PluginID, bus event.Manager) Plugin {
	switch id {
	case CONTACTRETRY:
		return NewContactRetry(bus)
	}

	return nil
}
