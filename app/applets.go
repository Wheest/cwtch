package app

import (
	"cwtch.im/cwtch/event"
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"sync"

	"cwtch.im/cwtch/app/plugins"
	"cwtch.im/cwtch/peer"
)

type appletPeers struct {
	peers    map[string]peer.CwtchPeer
	launched bool // bit hacky, place holder while we transition to full multi peer support and a better api
}

type appletACN struct {
	acn connectivity.ACN
}

type appletPlugins struct {
	plugins sync.Map //map[string] []plugins.Plugin
}

// ***** applet ACN

func (a *appletACN) init(acn connectivity.ACN, publish func(int, string)) {
	a.acn = acn
	acn.SetStatusCallback(publish)
	prog, status := acn.GetBootstrapStatus()
	publish(prog, status)
}

func (a *appletACN) Shutdown() {
	a.acn.Close()
}

// ***** appletPeers

func (ap *appletPeers) init() {
	ap.peers = make(map[string]peer.CwtchPeer)
	ap.launched = false
}

// LaunchPeers starts each peer Listening and connecting to peers and groups
func (ap *appletPeers) LaunchPeers() {
	log.Debugf("appletPeers LaunchPeers\n")
	if ap.launched {
		return
	}
	for _, p := range ap.peers {
		p.Listen()
		p.StartPeersConnections()
		p.StartGroupConnections()
	}
	ap.launched = true
}

// ListPeers returns a map of onions to their profile's Name
func (ap *appletPeers) ListPeers() map[string]string {
	keys := map[string]string{}
	for k, p := range ap.peers {
		keys[k] = p.GetProfile().Name
	}
	return keys
}

// GetPeer returns a cwtchPeer for a given onion address
func (ap *appletPeers) GetPeer(onion string) peer.CwtchPeer {
	if peer, ok := ap.peers[onion]; ok {
		return peer
	}
	return nil
}

// ***** applet Plugins

func (ap *appletPlugins) Shutdown() {
	ap.plugins.Range(func(k, v interface{}) bool {
		plugins := v.([]plugins.Plugin)
		for _, plugin := range plugins {
			plugin.Shutdown()
		}
		return true
	})
}

func (ap *appletPlugins) AddPlugin(peerid string, id plugins.PluginID, bus event.Manager) {
	if _, exists := ap.plugins.Load(peerid); !exists {
		ap.plugins.Store(peerid, []plugins.Plugin{})
	}

	pluginsinf, _ := ap.plugins.Load(peerid)
	peerPlugins := pluginsinf.([]plugins.Plugin)

	newp := plugins.Get(id, bus)
	newp.Start()
	peerPlugins = append(peerPlugins, newp)
	ap.plugins.Store(peerid, peerPlugins)
}
